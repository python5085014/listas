nombres = ["juan", "maria", "jose", "rosa"]

# para saber el largo de nuestra lista
print(len(nombres))

# agregar un nuevo dato a nuestra lista
nombres.append("pedro")

# insertar un elemento en un indice especifico
nombres.insert(1,"romina")
print(nombres)

# para remover un elemento de nuestra lista
nombres.remove("jose")
print(nombres)

# para remover un elemento por medio de su indice
nombres.pop(1)
print(nombres)


# ---------------------EJERCICIOS-----------------------

# sintaxis de range(inicial<opcional>, fin<requerido>, incremento<opcional>)

# 1. Iterar un rango de 0 a 10 e imprimir los numeros divisibles entre 3
print("Primer Ejercicio")
for i in range(11):
    if i % 3 == 0 and i != 0:
        print(i)

# 2. Crear un rango de numeros de 2 a 6, e imprimelos
print("Segundo Ejercicio")
for i in range(2,7):
    print(i)

# 3. Crear un rango de 3 al 10, pero con un incremento de 2 en 2
print("Tercer Ejercicio")
for i in range(3,11,2):
    print(i)